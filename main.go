package main

import (
	"errors"
	"fmt"
	"log/slog"
	"math"
)

var msg string

// always runs before main
func init() {
	msg = "from init()"
}

func main() {
	//////////////////////////////// basics

	fmt.Println(msg)

	var a, _, c = 1, 2, 3
	fmt.Println(a, c)

	var d, e int
	func() {
		d, e = 5, 10
		fmt.Println(d, e)
	}()
	fmt.Println(d, e)

	//////////////////////////////// if

	var agePeriodStr string
	var err error
	agePeriodStr, err = agePeriod(22)
	if err != nil {
		slog.Error(err.Error())
	}
	fmt.Println(agePeriodStr)

	agePeriodStr, err = agePeriod(27)
	fmt.Println(agePeriodStr)

	agePeriodStr, err = agePeriod(55)
	fmt.Println(agePeriodStr)

	agePeriodStr, err = agePeriod(101)
	fmt.Println(agePeriodStr)

	//////////////////////////////// switch case

	var trafficLightStr string
	var canGo bool

	trafficLightStr, canGo, err = trafficLight("light")
	if err != nil {
		slog.Error(err.Error())
	}

	fmt.Println(trafficLightStr, canGo)

	trafficLightStr, canGo, err = trafficLight("green")
	fmt.Println(trafficLightStr, canGo)

	//////////////////////////////// for

	fmt.Println(findMin(22, 3, 13, -15, -17, 2, 3, -2, -3))
	fmt.Println(min(22, 3, 13, -15, -17, 2, 3, -2, -3))
	fmt.Println(max(22, 3, 13, -15, -17, 2, 3, -2, -3))

	//////////////////////////////// Замыкание

	inc := increment() // Состояние переменной count сохраняется и с каждым разом увеличивается на 1
	fmt.Println(inc())
	fmt.Println(inc())
	fmt.Println(inc())
	fmt.Println(inc())
	fmt.Println(inc())

	//////////////////////////////// Указатели, ссылки

	message := "test"
	fmt.Println(message)
	changeMessage(&message, "testx2")
	fmt.Println(message)

	var p *int
	number := 10
	p = &number
	fmt.Println(*p, number)

	*p = 100
	fmt.Println(*p, number)

	number = 1000
	fmt.Println(*p, number)

	//////////////////////////////// arrays, slices

	messagesArray := [3]string{"1", "2", "3"}
	messagesArray[1] = "5"
	fmt.Println(messagesArray)
	//printMessages(messagesArray)
	fmt.Println(messagesArray)

	messagesSlice := []string{"1", "2", "3", "4", "5"}
	fmt.Println(messagesSlice)
	err = printMessages(messagesSlice)
	if err != nil {
		return
	}
	fmt.Println(messagesSlice)

	//var messages []string
	//messages[0] = "1" // Получим ошибку, т.к. под капотом слайс хранит указать на массив длина которого равна 0 после инициализации его пустым

	messages := make([]string, 5) // инициализируем слайс с 5 нулевыми значениями типа string. 3 параметром можно указать Capacity
	messages[0] = "10"
	fmt.Println(len(messages))
	fmt.Println(cap(messages))
	messages = append(messages, "6") // Происходит переаллокация в памяти и создается массив длинной каждый раз в 2 раза больше предыдущего
	// С какой то точки Go увеличивает массив не в 2 раза, а понемногу для экономии памяти
	fmt.Println(len(messages))
	fmt.Println(cap(messages))

	//////////////////////////////// matrix, for

	counter := 0
	matrix := make([][]int, 10)
	for x := 0; x < 10; x++ {
		matrix[x] = make([]int, 10)
		for y := 0; y < 10; y++ {
			counter++
			matrix[x][y] = counter
		}
		fmt.Println(matrix[x])
	}

	msgs := []string{
		"message1",
		"message2",
		"message3",
		"message4",
		"message5",
	}
	for i, value := range msgs {
		fmt.Println(i, value)
	}

	counter = 0
	for {
		if counter == 100 {
			break
		}
		counter++
		fmt.Println(counter)
	}

	//////////////////////////////// panic, defer, recover

	//panic("panic") // Непредвиденная/экстренная остановка программы

	//defer func() { // Отложенный вызов функции. Будет вызван в конце программы где бы не находился, вызов defer +50ms к выполнению программы
	//	fmt.Println("Bye")
	//}()

	// Recover is a built-in function that regains control of a panicking goroutine.
	// Recover is only useful inside deferred functions. During normal execution, a call to recover will return nil and have no other effect.
	// If the current goroutine is panicking, a call to recover will capture the value given to panic and resume normal execution.

	//////////////////////////////// Maps

	users := map[string]int{ // dictionary
		"key":    120,
		"nastya": 20,
	}
	fmt.Println(users)
	fmt.Println(users["nastya"]) // Если попытаться вывести значение ключа которого нет - получим нулевое занчение int конкретно здесь

	keyThatNotExists := "notExists"
	age, exists := users[keyThatNotExists]
	if exists {
		fmt.Println(users[keyThatNotExists], age)
	} else {
		fmt.Println("user not exists")
	}

	users["stepan"] = 20

	for key, value := range users {
		fmt.Println(key, value)
	}

	delete(users, "key")
	fmt.Println(users)

	var newUsers = make(map[string]int)
	newUsers["vasya"] = 25
	fmt.Println(newUsers)
	fmt.Println(len(newUsers))

	//////////////////////////////// struct

	//user := struct{
	//	name   string
	//	age    int
	//	sex    string
	//	weight float32
	//	height float32
	//}{"Vasya", 25, "Male", 75, 180}

	//user := User{"Vasya", 25, "Male", 75, 180}

	user := NewUser("Vasya", "Male", 25, 75, 180)

	fmt.Println(user)         // значения
	fmt.Printf("%+v\n", user) // поле - значение
	fmt.Println(user.name)
	//printUserInfo(user)
	user.printUserInfo()
	fmt.Println(user.getName())
	user.setName("Randy")
	fmt.Println(user.getName())
	fmt.Println(user.age.isAdult())

	//////////////////////////////// interface

	square := Square{8}
	circle := Circle{4}

	printShapeArea(square)
	printShapeArea(circle)

	printShapePerimeter(square)
	printShapePerimeter(circle)

	//printInterface(square)
	//printInterface(circle)

	printInterface("qwerty")
	printInterface(231)
}

// function that need to be exported starts with Capital Letter

func agePeriod(age uint8) (string, error) {
	if age >= 25 && age <= 44 {
		return "Молодой возраст(молодость)", nil
	} else if age >= 45 && age <= 59 {
		return "Средний возраст(зрелость)", nil
	} else if age >= 60 && age <= 74 {
		return "Пожилой возраст", nil
	} else if age >= 75 && age <= 89 {
		return "Стерческий возраст(старость)", nil
	} else if age >= 90 {
		return "Возраст долгожителей", nil
	}

	return "Роспотребнадзор не решил кто вы", errors.New("age not indicated in the order")
}

func trafficLight(light string) (string, bool, error) {
	switch light {
	case "red":
		return "Стой!", false, nil
	case "yellow":
		return "Приготовся!", false, nil
	case "green":
		return "Проходи", true, nil
	default:
		return "Прозиошла ошибка", false, errors.New(fmt.Sprintf("trafficLight light can't be %s", light))
	}
}

func findMin(numbers ...int) int {
	if len(numbers) == 0 {
		return 0
	}

	minNum := numbers[0]
	for _, value := range numbers {
		if value < minNum {
			minNum = value
		}
	}

	return minNum
}

// Замыкание
// increment returns function that returns int
func increment() func() int {
	count := 0
	return func() int {
		count++
		return count
	}
}

// Ссылки, указатели
// The * symbol is used to declare a pointer and to dereference
// The & symbol points to the address of the stored value
func changeMessage(msg *string, value string) {
	*msg += value
}

func printMessages(messages []string) error {
	if len(messages) == 0 {
		return errors.New("empty array/slice")
	}

	messages[2] = "10" // original array won't be changed, BUT SLICE WILL

	fmt.Println(messages)
	return nil
}

//////////////////////////////// Structures

// User struct
type User struct {
	name   string
	age    Age
	sex    string
	weight float32
	height float32
}

// Будет методом структуры User. u User Здесь является receiver 'ом. Существуют value и pointer ресиверы
// При создании метода по значению при попытке, например, изменить u.name ничего не произойдет
// Но если указать u *User, то значение поле при попытке изменить будет меняться в оригинальном объекте
func (u User) printUserInfo() {
	fmt.Println(u.name, u.age, u.sex, u.weight, u.height)
}

// Указатель для возможности изменения поля
func (u *User) setName(name string) {
	u.name = name
}

// Без указателя, т.к. поле не надо менять
func (u User) getName() string {
	return u.name
}

func NewUser(name, sex string, age int, weight, height float32) User {
	return User{
		name:   name,
		sex:    sex,
		age:    Age(age),
		weight: weight,
		height: height,
	}
}

type DumbDatabase struct {
	m map[string]string
}

func NewDumbDatabase() *DumbDatabase {
	return &DumbDatabase{
		m: make(map[string]string), // инициируем Map для дальнейшей работы с ним
	}
}

//func printUserInfo(user User) {
//	fmt.Println(user.name, user.age, user.sex, user.weight, user.height)
//}

// Age Custom types
// Для встроенного типа int не можем создавать методы, поэтмоу можно сделать так
type Age int

func (a Age) isAdult() bool {
	return a >= 18
}

//////////////////////////////// Interfaces

type Shape interface {
	//Area() float32
	ShapeWithArea
	ShapeWithPerimeter
}

type ShapeWithArea interface {
	Area() float32
}

type ShapeWithPerimeter interface {
	Perimeter() float32
}

type Square struct {
	sideLength float32
}

func (s Square) Area() float32 {
	return s.sideLength * s.sideLength
}

func (s Square) Perimeter() float32 {
	return s.sideLength * 4
}

type Circle struct {
	radius float32
}

func (c Circle) Area() float32 {
	return c.radius * c.radius * math.Pi
}

func (c Circle) Perimeter() float32 {
	return 2 * math.Pi * c.radius
}

func printShapeArea(shape Shape) {
	fmt.Println(shape.Area())
}

func printShapePerimeter(shape Shape) {
	fmt.Println(shape.Perimeter())
}

// пустой interface
//
//	func printInterface(i interface{}) {
//		fmt.Printf("%+v\n", i)
//	}
//
//	func printInterface(i interface{}) {
//		switch value := i.(type) {
//		case int:
//			fmt.Println("int", value)
//		case bool:
//			fmt.Println("bool", value)
//		default:
//			fmt.Println("unknown type")
//		}
//		fmt.Printf("%+v\n", i)
//	}
func printInterface(i interface{}) {
	str, ok := i.(string) // Приведение типов
	if !ok {
		fmt.Println("interface is not a string")
	}
	fmt.Println(str)
}
